# Docker Testings

This container was built from the [Docker getting started tutorial](https://docs.docker.com/get-started/).

## Project Structure

```no-highlight
/
├── app.py: The core testing python app.
├── docker-compose.yml: The YML compose file describing the stack being deployed
├── Dockerfile: The file describing the docker container.
└── requirements.txt: The file containing all python requirements to be installed by pip.
```

